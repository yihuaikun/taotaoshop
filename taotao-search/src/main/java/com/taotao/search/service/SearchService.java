package com.taotao.search.service;

import com.taotao.pojo.SearchResult;

/**
 * @author yhk
 * @create 2019-11-19 22:41
 */
public interface SearchService {
    /**
     * 商品搜索服务
     * @param keywords
     * @param pageNum
     * @param pageSize
     * @return
     */
    public SearchResult search(String keywords, Integer pageNum, Integer pageSize);
}
