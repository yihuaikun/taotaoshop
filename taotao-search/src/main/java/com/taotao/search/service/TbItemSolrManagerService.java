package com.taotao.search.service;

import com.taotao.common.TaotaoResult;

/**
 * @author yhk
 * @create 2019-11-19 21:57
 */
public interface TbItemSolrManagerService {
    /**
     * 导入数据到solr
     * @return
     */
    public TaotaoResult importAllTbItem2Solr();
}
