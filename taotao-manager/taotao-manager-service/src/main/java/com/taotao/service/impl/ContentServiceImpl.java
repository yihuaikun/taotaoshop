package com.taotao.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.taotao.common.EasyUIResult;
import com.taotao.common.HttpUtil;
import com.taotao.common.TaotaoResult;
import com.taotao.mapper.TbContentMapper;
import com.taotao.pojo.TbContent;
import com.taotao.pojo.TbContentExample;
import com.taotao.service.ContentService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author yhk
 * @create 2019-11-15 19:04
 */
@Service
public class ContentServiceImpl implements ContentService {
    @Autowired
    private TbContentMapper tbContentMapper;
    /**
     * 点击分类内容查询对应所有结果
     * @param categoryId
     * @param page
     * @param rows
     * @return
     */
    @Override
    public EasyUIResult QueryContentList(Long categoryId, int page, int rows) {
        //设置分页参数
        PageHelper.startPage(page,rows);
        //设置查询条件
        TbContentExample example = new TbContentExample();
        example.createCriteria().andCategoryIdEqualTo(categoryId);
        List<TbContent> contentList = tbContentMapper.selectByExample(example);
        PageInfo<TbContent> pageInfo =  new PageInfo<>(contentList);
        long total = pageInfo.getTotal();
        return new EasyUIResult(total,contentList);
    }

    /**
     * 更新分类内容信息
     * @param contentCon
     * @return
     */
    @Override
    public TaotaoResult updateContent(TbContent contentCon) {
        contentCon.setCreated(new Date());
        contentCon.setUpdated(new Date());
        //创建更新条件
        TbContentExample example = new TbContentExample();
        example.createCriteria().andIdEqualTo(contentCon.getId());
        tbContentMapper.updateByExampleSelective(contentCon,example);
        HttpUtil.doGet("http://localhost:8081/cache/clear/content/cat/"+contentCon.getCategoryId()+"");
        return TaotaoResult.ok();
    }

    /**
     * 批量删除内容
     * @param delList
     * @return
     */
    @Override
    public TaotaoResult batchDeletsContent(List<String> delList) {
        tbContentMapper.batchDeletsContent(delList);

        return TaotaoResult.ok();
    }

    /**
     * 删除分类内容
     * @param ids
     * @return
     */
    @Override
    public TaotaoResult deletConTent(String ids) {
        Long id = Long.valueOf(ids);
        tbContentMapper.deleteByPrimaryKey(id);
        return TaotaoResult.ok();
    }

    /**
     * 新增分类内容
     * @param content
     * @return
     */
    @Override
    public TaotaoResult saveContent(TbContent content) {
        content.setUpdated(new Date());
        content.setCreated(new Date());
        tbContentMapper.insert(content);
        Long id = content.getCategoryId();
        HttpUtil.doGet("http://localhost:8081/cache/clear/content/cat/"+id+"");
        return TaotaoResult.ok();
    }
}
