package com.taotao.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @author yhk
 * @create 2019-11-12 11:18
 */
public interface PictureService {
   Map<String, Object> uploadPicture(MultipartFile picFile);
}
