package com.taotao.service;

import com.taotao.pojo.EasyUITreeNode;

import java.util.List;

/**
 * @author yhk
 * @create 2019-11-11 19:03
 */
public interface TbItemCatService {
    /**
     *返回栏目列表
     * @param parentId
     * @return
     */
    public List<EasyUITreeNode> getCatList(long parentId);
}
