package com.taotao.service;

import com.taotao.common.EasyUIResult;
import com.taotao.common.TaotaoResult;
import com.taotao.pojo.TbContent;

import java.util.List;

/**
 * @author yhk
 * @create 2019-11-15 19:04
 */
public interface ContentService {

    /**
     * 点击分类内容查询对应所有结果
     * @param categoryId
     * @param page
     * @param rows
     * @return
     */
    EasyUIResult QueryContentList(Long categoryId, int page, int rows);

    /**
     * 更新分类内容信息
     * @param contentCon
     * @return
     */
    TaotaoResult updateContent(TbContent contentCon);

    /**
     * 批量删除分类内容
     * @param delList
     * @return
     */
    TaotaoResult batchDeletsContent(List<String> delList);

    /**
     *删除分类内容
     * @param ids
     * @return
     */
    TaotaoResult deletConTent(String ids);

    /**
     * 新增分类内容
     * @param content
     * @return
     */
    TaotaoResult saveContent(TbContent content);
}
