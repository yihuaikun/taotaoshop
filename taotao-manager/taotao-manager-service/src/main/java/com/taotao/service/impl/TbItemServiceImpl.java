package com.taotao.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.taotao.common.*;
import com.taotao.mapper.*;
import com.taotao.pojo.*;
import com.taotao.service.TbItemService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author yhk
 * @create 2019-11-10 22:47
 */

@Service
public class TbItemServiceImpl implements TbItemService {
    private static Log log = LogFactory.getLog(TbItemServiceImpl.class);
    @Autowired
    private TbItemMapper tbItemMapper; //商品的mapper
    @Autowired
    private TbItemDescMapper tbItemDescMapper;//商品描述的mapper
    @Autowired
    private TbItemParamMapper tbItemParamMapper; //规格参数模板mapper
    @Autowired
    private TbItemCatMapper tbItemCatMapper;  //商品分类的mapper
    @Autowired
    private TbItemParamItemMapper tbItemParamItemMapper;  //商品的规格参数模板

    /**
     * 商品列表的分页查询
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public EasyUIResult getItemList(int pageNum, int pageSize) {
        // 设置分页参数
        PageHelper.startPage(pageNum, pageSize);
        List<TbItem> tbItemList = tbItemMapper.selectByExample(new TbItemExample());

        PageInfo<TbItem> pageInfo = new PageInfo<>(tbItemList);
        long total = pageInfo.getTotal();
        return new EasyUIResult(total, tbItemList);
    }

    /**
     * 获取商品编辑的详细信息
     * @param id
     * @return
     */
    @Override
    public TbItemDesc getItemDesc(Long id) {
        TbItemDesc tbItemDesc = tbItemDescMapper.selectByPrimaryKey(id);

        return tbItemDesc;
    }

    /**
     * 删除商品
     * @param itemId
     * @return
     */
    @Override
    public TaotaoResult deleteItem(Long itemId) {
        TbItem tbItem = new TbItem();
        tbItem.setId(itemId);
        tbItem.setStatus((byte)3);
        tbItemMapper.updateByPrimaryKeySelective(tbItem);
        return TaotaoResult.ok();
    }

    /**
     * 商品的上架/下架
     * @param ids
     * @param method
     * @return
     */
    @Override
    public TaotaoResult updateItemStatus(List<Long> ids, String method) {
        TbItem item = new TbItem();
        if(method.equals("reshelf")) {
            item.setStatus((byte) 1);
        }else if(method.equals("instock")) {
            item.setStatus((byte) 2);
        }

        for (Long id : ids) {
            TbItemExample tbItemExample = new TbItemExample();
            TbItemExample.Criteria criteria = tbItemExample.createCriteria();
            criteria.andIdEqualTo(id);
            tbItemMapper.updateByExampleSelective(item, tbItemExample);
        }
        return TaotaoResult.ok();
    }

    /**
     * 商品添加
     * @param tbItem
     * @param desc
     * @return
     */
    @Transactional(isolation = Isolation.READ_COMMITTED,propagation = Propagation.REQUIRED)
    @Override
    public TaotaoResult saveTbItem(TbItem tbItem, String desc,String itemParams) {
       try {
           long itemId = IDUtils.genItemId();
           //获取关于商品的参数
           tbItem.setId(itemId);
           tbItem.setStatus(SystemConstants.TAOTAO_ITEM_STATUS_NORMAL);
           tbItem.setCreated(new Date());
           tbItem.setUpdated(new Date());
           tbItemMapper.insert(tbItem);

           //封装商品详情信息
           TbItemDesc tbItemDesc =  new TbItemDesc();
           tbItemDesc.setItemDesc(desc);
           tbItemDesc.setCreated(new Date());
           tbItemDesc.setUpdated(new Date());
           tbItemDesc.setItemId(tbItem.getId());
           tbItemDescMapper.insert(tbItemDesc);

           //封装商品规格信息
           TbItemParamItem itemParamItem = new TbItemParamItem();
           itemParamItem.setItemId(tbItem.getId());
           itemParamItem.setParamData(itemParams);
           itemParamItem.setCreated(new Date());
           itemParamItem.setUpdated(new Date());
           tbItemParamItemMapper.insert(itemParamItem);
           return TaotaoResult.ok();


       }catch (Exception e){
           log.error("保存失败",e);
           return TaotaoResult.build(SystemConstants.TAOTAO_RESULT_STATUS_ERROR, ExceptionUtil.getStackTrace(e));
       }

    }

    /**
     * 编辑商品信息
     * @param tbItem
     * @param desc
     * @return
     */
    @Override
    public TaotaoResult updateTbItem(TbItem tbItem, String desc,String itemParams) {
        try {
            //获取前台数据传过来的关于该商品的ID,根据商品的id执行修改商品信息和修改描述信息
            Long itemId = tbItem.getId();
            //根据id查询当前的销售状态以及一些其他的必要数据进行封装
            TbItem tbItem1 = tbItemMapper.selectByPrimaryKey(itemId);
            tbItem.setStatus(tbItem1.getStatus());
            tbItem.setCreated(tbItem1.getCreated());
            tbItem.setUpdated(tbItem1.getUpdated());
            //执行修改操作
            tbItemMapper.updateByPrimaryKey(tbItem);
            //关联关于该商品的描述信息
            TbItemDesc tbItemDesc = new TbItemDesc();
            tbItemDesc.setItemId(itemId);
            tbItemDesc.setItemDesc(desc);
            tbItem.setUpdated(new Date());
            tbItem.setCreated(tbItem.getCreated());
            tbItemDescMapper.updateByPrimaryKey(tbItemDesc);
            //封装商品规格信息
            TbItemParamItem itemParamItem = new TbItemParamItem();
           /* itemParamItem.setItemId(tbItem.getId());*/
         /*  //查询这条记录的id
            Long id = tbItemParamItemMapper.queryId(tbItem.getId());
            System.out.println(id);
            itemParamItem.setId(id);*/
         /*  TbItemParamExample tbItemParamExample = new TbItemParamExample();
           tbItemParamExample.createCriteria().*/
            TbItemParamItemExample tbItemParamExample = new TbItemParamItemExample();
            tbItemParamExample.createCriteria().andItemIdEqualTo(tbItem.getId());
            itemParamItem.setParamData(itemParams);
            itemParamItem.setCreated(new Date());
            itemParamItem.setUpdated(new Date());
            tbItemParamItemMapper.updateByExampleSelective(itemParamItem,tbItemParamExample);
            return TaotaoResult.ok();

        }catch (Exception e){
            log.error("编辑失败",e);
            return TaotaoResult.build(SystemConstants.TAOTAO_RESULT_STATUS_ERROR, ExceptionUtil.getStackTrace(e));
        }

    }

    /**
     * 获取商品规格模板列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public EasyUIResult getTbItemParamList(int pageNum, int pageSize) {
        // 设置分页参数
        PageHelper.startPage(pageNum, pageSize);
        List<TbItemParam> tbItemList = tbItemParamMapper.selectByExampleWithBLOBs(new TbItemParamExample());
        for (TbItemParam tbItemParam : tbItemList) {
            TbItemCat tbItemCat = tbItemCatMapper.selectByPrimaryKey(tbItemParam.getItemCatId());
            tbItemParam.setItemCatName(tbItemCat.getName());
        }
        PageInfo<TbItemParam> pageInfo = new PageInfo<>(tbItemList);
        long total = pageInfo.getTotal();
        return new EasyUIResult(total, tbItemList);
    }

    /**
     * 保存规格参数模板
     * @param itemCatId
     * @param paramData
     * @return
     */
    @Override
    public TaotaoResult saveTbItemParam(Long itemCatId, String paramData) {
        TbItemParam tbItemParam = new TbItemParam();
        tbItemParam.setItemCatId(itemCatId);
        tbItemParam.setParamData(paramData);
        tbItemParam.setCreated(new Date());
        tbItemParam.setUpdated(new Date());
        tbItemParamMapper.insert(tbItemParam);
        return TaotaoResult.ok();
    }
    /**
     * 添加商品套用模板
     * @param id
     * @return
     */
    @Override
    public TaotaoResult getItemParamByItemCatId(Long id) {
        TbItemParamExample condition = new TbItemParamExample();
        condition.createCriteria().andItemCatIdEqualTo(id);
        List<TbItemParam> tbItemParamList = tbItemParamMapper.selectByExampleWithBLOBs(condition);
        if (tbItemParamList.size() > 0) {
            return TaotaoResult.ok(tbItemParamList.get(0));
        }
        return TaotaoResult.build(SystemConstants.TAOTAO_RESULT_STATUS_ERROR,"数据未查询到");
    }

    /**
     * 删除规格参数模板
     * @param ids
     * @return
     */
    @Override
    public TaotaoResult deleteItemParam(Long ids) {
        tbItemParamMapper.deleteByPrimaryKey(ids);
        return TaotaoResult.ok();
    }
    /**
     * 编辑商品时查询商品对应的规格参数
     * @param id
     * @return
     */
    @Override
    public TaotaoResult queryItemParamByItemId(Long id) {
        TbItemParamItemExample condition = new TbItemParamItemExample();
        condition.createCriteria().andItemIdEqualTo(id);
        List<TbItemParamItem> tbItemParamItems = tbItemParamItemMapper.selectByExampleWithBLOBs(condition);
        if(tbItemParamItems.size() > 0){
            return TaotaoResult.ok(tbItemParamItems.get(0));
        }
        return TaotaoResult.build(SystemConstants.TAOTAO_RESULT_STATUS_ERROR,"未查询到数据");
    }

    /**
     * 更新商品规格信息
     * @param cid
     * @param paramData
     * @return
     */
    @Override
    public TaotaoResult updateItemParamByItemCId(Long cid, String paramData) {
        TbItemParam tbItemParam = new TbItemParam();
        tbItemParam.setParamData(paramData);
        tbItemParam.setUpdated(new Date());
        TbItemParamExample tbItemParamExample = new TbItemParamExample();
        tbItemParamExample.createCriteria().andItemCatIdEqualTo(cid);
        tbItemParamMapper.updateByExampleSelective(tbItemParam, tbItemParamExample);
        return TaotaoResult.ok();
    }

    /**
     * 批量删除商品
     * @param delList
     * @return
     */
    @Override
    public TaotaoResult batchDeletsItem(List<String> delList) {
        tbItemParamMapper.batchDeletsContent(delList);
        return TaotaoResult.ok();
    }

    /**
     * 查询单个商品的信息
     * @param itemId
     * @return
     */
    @Override
    public TaotaoResult selectOneItem(Long itemId) {
        TbItem tbItem = tbItemMapper.selectByPrimaryKey(itemId);
        return TaotaoResult.ok(tbItem);
    }
//    /**
//     * 保存商品时保存规格参数信息
//     * @param itemId
//     * @param itemParams
//     */
//    @Override
//    public void saveTbItemParamData(Long itemId, String itemParams) {
//        try {
//            //将规格参数字符串转换成对象
//            //创建ObjectMapper对象
//            ObjectMapper mapper = new ObjectMapper();
////            TbItemParamItem item = new TbItemParamItem();
//            //转换为我们需要的java对象
//            TbItemParamItem item = mapper.readValue(itemParams,TbItemParamItem.class);
//            item.setItemId(itemId);
//            System.out.println(item.toString());
//            /*//在把对象转换成json字符串进行数据的封装
//            String jsonitemParams = mapper.writeValueAsString(item);*/
//            //执行保存操作
//           tbItemParamMapper.insert(item);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
