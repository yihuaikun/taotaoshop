package com.taotao.service;

import com.taotao.common.EasyUIResult;
import com.taotao.common.TaotaoResult;
import com.taotao.pojo.TbItem;
import com.taotao.pojo.TbItemDesc;

import java.util.List;

/**
 * 商品的业务层接口
 * @author yhk
 * @create 2019-11-10 22:46
 */
public interface TbItemService {
    /**
     * 处理结果集
     * @param pageNum
     * @param pageSize
     * @return
     */
    public EasyUIResult getItemList(int pageNum, int pageSize);

    /**
     * 查询商品描述信息
     * @param id
     * @return
     */
    TbItemDesc getItemDesc(Long id);

    /**
     * 删除商品信息
     * @param itemId
     * @return
     */
    TaotaoResult deleteItem(Long itemId);

    /**
     * 更新商品状态  上架 and 下架
     * @param ids
     * @param method
     * @return
     */
    TaotaoResult updateItemStatus(List<Long> ids, String method);

    /**
     * 商品的添加
     * @param tbItem
     * @param desc
     * @return
     */
    TaotaoResult saveTbItem(TbItem tbItem, String desc,String itemParams);

    /**
     * 修改商品的操作
     * @param tbItem
     * @param desc
     */
    TaotaoResult updateTbItem(TbItem tbItem, String desc,String itemParams);

    /**
     * 查询所有规格参数模板
     * @param pageNum
     * @param pageSize
     * @return
     */
    EasyUIResult getTbItemParamList(int pageNum, int pageSize);

    /**
     * 保存商品规格参数模板
     * @param itemCatid
     * @param paramData
     * @return
     */
    TaotaoResult saveTbItemParam(Long itemCatid, String paramData);

    /**
     * 添加商品套用模板
     * @param id
     * @return
     */
    TaotaoResult getItemParamByItemCatId(Long id);

    /**
     * 删除商品规格模板
     * @param ids
     * @return
     */
    TaotaoResult deleteItemParam(Long ids);
    /**
     * 编辑商品时查询商品对应的规格参数
     * @param id
     * @return
     */
    TaotaoResult queryItemParamByItemId(Long id);

    /**
     * 修改商品规格信息
     * @param cid
     * @param paramData
     * @return
     */
    TaotaoResult updateItemParamByItemCId(Long cid, String paramData);

    /**
     * 批量删除商品
     * @param delList
     * @return
     */
    TaotaoResult batchDeletsItem(List<String> delList);

    /**
     * 查询单个商品
     * @param itemId
     * @return
     */
    TaotaoResult selectOneItem(Long itemId);

    /**
     * 保存商品时保存规格参数信息
     * @param itemId
     * @param itemParams
     */
//    void saveTbItemParamData(Long itemId, String itemParams);
}
