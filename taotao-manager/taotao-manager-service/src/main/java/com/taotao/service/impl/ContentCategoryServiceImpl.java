package com.taotao.service.impl;

import com.taotao.common.TaotaoResult;
import com.taotao.mapper.TbContentCategoryMapper;
import com.taotao.pojo.EasyUITreeNode;
import com.taotao.pojo.TbContentCategory;
import com.taotao.pojo.TbContentCategoryExample;
import com.taotao.service.ContentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author yhk
 * @create 2019-11-15 11:53
 */
@Service
public class ContentCategoryServiceImpl implements ContentCategoryService {
    @Autowired
    private TbContentCategoryMapper tbContentCategoryMapper;

    /**
     * 查询所有内容
     * @param parendid
     * @return
     */
    @Override
    public List<EasyUITreeNode> contentCategoryService(Long parendid) {
        //根据parentId查询内容分类列表
        TbContentCategoryExample example = new TbContentCategoryExample();
        //创建查询条件
        TbContentCategoryExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(parendid).andStatusEqualTo(1);
        List<TbContentCategory> list = tbContentCategoryMapper.selectByExample(example);
        List<EasyUITreeNode> resultList = new ArrayList<>();
        for (TbContentCategory tbContentCategory : list) {
            EasyUITreeNode node = new EasyUITreeNode();
            node.setId(tbContentCategory.getId());
            node.setText(tbContentCategory.getName());
           //判断是否是父节点
            if(tbContentCategory.getIsParent()) {
                node.setState("closed");

            }else {
                node.setState("open");
            }
            //循环一次添加一次,将封装好的树对象添加到List中返回
            resultList.add(node);
        }
        return resultList;
    }

    /**
     * 添加节点
     * @param parentId
     * @param name
     * @return
     */
    @Override
    public TaotaoResult addNode(Long parentId, String name) {
        Date date = new Date();
        //需求:在父节点下添加新的子节点
        //创建一个节点对象
        TbContentCategory node = new TbContentCategory();
        //设置各项必要参数
        node.setName(name);
        node.setParentId(parentId);
        node.setCreated(date);
        node.setUpdated(date);
        //设置排列序号
        node.setSortOrder(1);
        //设置节点状态 可选值:1(正常),2(删除)
        node.setStatus(1);
        node.setIsParent(false);
        //插入新节点.需要返回主键
        tbContentCategoryMapper.insert(node);
        //判断如果父节点的isparent是不是true,不是就修改true
        //取父节点的内容
        TbContentCategory tbContentCategory = tbContentCategoryMapper.selectByPrimaryKey(parentId);
        if(!tbContentCategory.getIsParent()) {
            tbContentCategory.setIsParent(true);
            tbContentCategoryMapper.updateByPrimaryKey(tbContentCategory);
        }
        //返回新的节点
        return TaotaoResult.ok(node);
    }

    /**
     * 删除父节点或者子节点
     * @param id
     * @return
     */
    @Override
    public TaotaoResult deleteNode(Long id) {
        //需求分析 : 首先要根据参数id查询这条记录对应的值
        TbContentCategory tbContentCategory = tbContentCategoryMapper.selectByPrimaryKey(id);
        Boolean isParent = tbContentCategory.getIsParent();
        //创建查询条件
        TbContentCategoryExample example = new TbContentCategoryExample();
        TbContentCategoryExample.Criteria criteria = example.createCriteria();
        //是父节点
        if(isParent == true) {
            //先删除它下面的所有子节点
            tbContentCategoryMapper.updateStatusByParentId(id);
            //设置状态等于2
            tbContentCategory.setStatus(2);
            //在删除父节点
            criteria.andIdEqualTo(id);
            tbContentCategoryMapper.updateByExample(tbContentCategory,example);
         }else {
                //设置状态等于2
                tbContentCategory.setStatus(2);
                //如果不是则就删除单条节点
                criteria.andIdEqualTo(id);
                tbContentCategoryMapper.updateByExample(tbContentCategory,example);
            }
        return TaotaoResult.ok();
    }

    @Override
    public TaotaoResult updateNode(Long id, String name) {
        //修改分析,首先根据id查询要修改的节点
        TbContentCategory tbContentCategory = tbContentCategoryMapper.selectByPrimaryKey(id);
        tbContentCategory.setName(name);
        //开始更新名称
        tbContentCategoryMapper.updateByPrimaryKey(tbContentCategory);
        return TaotaoResult.ok();
    }
}
