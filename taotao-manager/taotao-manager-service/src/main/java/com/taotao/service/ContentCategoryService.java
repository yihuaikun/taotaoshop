package com.taotao.service;

import com.taotao.common.TaotaoResult;
import com.taotao.pojo.EasyUITreeNode;

import java.util.List;

/**
 * @author yhk
 * @create 2019-11-15 11:46
 */
public interface  ContentCategoryService {
    /**
     *分类内容列表展示
     * @param parendid
     * @return
     */
    public  List<EasyUITreeNode> contentCategoryService(Long parendid);

    /**
     * 新增子节点
     * @param parentId
     * @param name
     * @return
     */
    public  TaotaoResult addNode(Long parentId, String name);

    /**
     * 删除整个分类或者节点
     * @param id
     * @return
     */
    public  TaotaoResult deleteNode(Long id);

    /**
     * 更新节点名称
     * @param id
     * @param name
     * @return
     */
    TaotaoResult updateNode(Long id, String name);
}
