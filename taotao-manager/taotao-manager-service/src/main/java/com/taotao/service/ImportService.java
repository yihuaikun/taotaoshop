package com.taotao.service;

import com.taotao.common.TaotaoResult;

/**
 * @author yhk
 * @create 2019-11-20 9:21
 */
public interface ImportService {
    TaotaoResult importItem();
}
