package com.taotao.service.impl;

import com.taotao.common.HttpUtil;
import com.taotao.common.TaotaoResult;
import com.taotao.service.ImportService;
import org.springframework.stereotype.Service;

/**
 * @author yhk
 * @create 2019-11-20 9:24
 */
@Service
public class ImportServiceImpl implements ImportService {

    private String IMPORT_BASE_URL = "http://localhost:8083";
    private  String IMPORT_URL = "/search/solr/manager/import_item" ;
    @Override
    public TaotaoResult importItem() {
        //调用服务导入数据
        try {
            String result = HttpUtil.doPost(IMPORT_BASE_URL + IMPORT_URL);
        } catch (Exception e) {
            e.printStackTrace();
            TaotaoResult.build(500,"数据导入失败");
        }
        return TaotaoResult.ok();
    }
}
