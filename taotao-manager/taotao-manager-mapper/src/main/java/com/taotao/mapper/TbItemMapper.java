package com.taotao.mapper;

import com.taotao.pojo.SearchItem;
import com.taotao.pojo.TbItem;
import com.taotao.pojo.TbItemExample;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface TbItemMapper {
    long countByExample(TbItemExample example);

    int deleteByExample(TbItemExample example);

    int deleteByPrimaryKey(Long id);

    int insert(TbItem record);

    int insertSelective(TbItem record);

    List<TbItem> selectByExample(TbItemExample example);

    TbItem selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") TbItem record, @Param("example") TbItemExample example);

    int updateByExample(@Param("record") TbItem record, @Param("example") TbItemExample example);

    int updateByPrimaryKeySelective(TbItem record);

    int updateByPrimaryKey(TbItem record);
    //自己定义
    int updateByPrimaryId(TbItem record);
    //搜索
    List<SearchItem> selectAllSearchItem();
}