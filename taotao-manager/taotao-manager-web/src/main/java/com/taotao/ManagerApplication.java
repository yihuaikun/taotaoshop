package com.taotao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author yhk
 * @create 2019-11-09 15:07
 */
@SpringBootApplication
@MapperScan("com.taotao.mapper")
public class ManagerApplication {
    public static void main(String[] args) {

        SpringApplication.run(ManagerApplication.class,args);
    }
}
