package com.taotao.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author yhk
 * @create 2019-11-09 17:17
 */
@Controller
public class PageController {
    /**
     * 展示首页
     * @return
     */
    @RequestMapping("/")
    public  String showIndex() {

        return  "index";
    }
    @RequestMapping("/{page}")
    public  String  showPage(@PathVariable String page) {
        return page;
    }

    /*@RequestMapping("/page")
    public  String  showPage(@RequestParam String p) {
        return p;
    }*/
}
