package com.taotao.web.controller;

import com.taotao.common.TaotaoResult;
import com.taotao.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author yhk
 * @create 2019-11-20 9:13
 */
@Controller
public class ItemImportController {
    @Autowired
    private ImportService importService;

    @RequestMapping("/import")
    @ResponseBody
    public TaotaoResult showIndex() {
        TaotaoResult result = importService.importItem();
        return result;
    }
}
