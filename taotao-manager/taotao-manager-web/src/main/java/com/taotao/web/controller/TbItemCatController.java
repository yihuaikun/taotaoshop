package com.taotao.web.controller;

import com.taotao.pojo.EasyUITreeNode;
import com.taotao.service.TbItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author yhk
 * @create 2019-11-11 19:03
 */
@Controller
@RequestMapping("/item/cat")
public class TbItemCatController {
    @Autowired
    private TbItemCatService tbItemCatService;

    @RequestMapping("/list")
    @ResponseBody
    public List<EasyUITreeNode> getTbItemList(@RequestParam(value = "id", defaultValue = "0") long parentId) {
        List<EasyUITreeNode> result = tbItemCatService.getCatList(parentId);
        return result;
    }
}
