package com.taotao.web.controller;

import com.taotao.common.EasyUIResult;
import com.taotao.common.TaotaoResult;
import com.taotao.pojo.TbItem;
import com.taotao.pojo.TbItemDesc;
import com.taotao.service.TbItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品管理Controller
 * @author yhk
 * @create 2019-11-10 22:50
 */
@Controller
@RequestMapping("/item")
public class ItemController {
    @Autowired
    private TbItemService tbItemService;
    /**
     *查询所有商品
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public EasyUIResult getTbItemList(@RequestParam("page") int pageNum, @RequestParam("rows") int pageSize) {
        EasyUIResult result = tbItemService.getItemList(pageNum, pageSize);
        return result;
    }

    /**
     * 回显商品描述数据
     * @param id
     * @return
     */
    @RequestMapping("/queryDesc/{id}")
    @ResponseBody
    public TbItemDesc usePathVariable(@PathVariable("id") Long id){
        TbItemDesc TbItemDesc = tbItemService.getItemDesc(id);
        System.out.println(TbItemDesc);
        return TbItemDesc;

    }

    /**
     * 为购物车提供查单个商品查询服务
     * @param itemId
     * @return
     */
    @RequestMapping("/queryItem/{itemId}")
    @ResponseBody
    public TaotaoResult selectOneItem(@PathVariable("itemId") Long itemId) {
        TaotaoResult result = tbItemService.selectOneItem(itemId);
        return  result;
    }
    /**
     * 编辑商品时查询商品对应的规格参数
     * @param id
     * @return
     */
    @RequestMapping("/queryItemParamByItemId/{id}")
    @ResponseBody
    public TaotaoResult queryItemParamByItemId(@PathVariable("id") Long id){
        TaotaoResult result = tbItemService.queryItemParamByItemId(id);
        return result;
    }

    /**
     * 删除商品
     * @param ids
     * @return
     */
    @RequestMapping(value = "/deleteItem",method = RequestMethod.POST)
    @ResponseBody
    public TaotaoResult deleteItem(String ids){

        TaotaoResult result = null;
        List<String> delList = new ArrayList<String>();
        if (ids.contains(",")) {
            String[] strs = ids.split(",");
            for (String str : strs) {
                delList.add(str);
            }
            result = tbItemService.batchDeletsItem(delList);
        }else{
            result = tbItemService.deleteItem(Long.valueOf(ids));
        }
        return  result;
    }

    /**
     * 商品的上架/下架
     * @param ids
     * @param method
     * @return
     */
    @RequestMapping("/updateItemStatus/{method}")
    @ResponseBody
    public TaotaoResult updateItemStatus(@RequestParam(value="ids") List<Long> ids, @PathVariable String method) {
        TaotaoResult result = tbItemService.updateItemStatus(ids,method);
        return result;
    }

    /**
     * 商品的添加
     * @param tbItem
     * @param desc
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public TaotaoResult saveTbItem(TbItem tbItem,String desc,String itemParams) {
        System.out.println(itemParams);
        //保存商品信息
        TaotaoResult result = tbItemService.saveTbItem(tbItem,desc,itemParams);
        //获取要保存的商品信息
        Long itemId = tbItem.getId();
        //保存对应的规格参数信息
//        tbItemService.saveTbItemParamData(itemId,itemParams);
        return result;


    }
    /**
     * 商品的更新
     * @param tbItem
     * @param desc
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public TaotaoResult updateTbItem(TbItem tbItem,String desc,String itemParams) {
        TaotaoResult result = tbItemService.updateTbItem(tbItem, desc,itemParams);
        return result;
    }
    /**
     *查询规格参数列表
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/paramList")
    @ResponseBody
    public EasyUIResult getTbItemParamList(@RequestParam("page") int pageNum, @RequestParam("rows") int pageSize) {
        EasyUIResult result = tbItemService.getTbItemParamList(pageNum, pageSize);
        return result;
    }

    /**
     * 添加商品规格
     * @param itemCatId
     * @param paramData
     * @return
     */
    @RequestMapping("/saveTbItemParam/{itemCatId}")
    @ResponseBody
    public TaotaoResult saveTbItemParam(@PathVariable Long itemCatId,String paramData) {
        TaotaoResult result = tbItemService.saveTbItemParam(itemCatId,paramData);
        return  result;
    }

    /**
     * 添加商品套用模板
     * @return
     */
    @RequestMapping("/queryItemParamByItemCatId/{id}")
    @ResponseBody
    public TaotaoResult queryItemParamByItemCatId(@PathVariable Long id) {
        TaotaoResult result = tbItemService.getItemParamByItemCatId(id);
        return result;

    }

    /**
     * 删除商品规格模板
     * @param ids
     * @return
     */
    @RequestMapping("/deleteItemParam")
    @ResponseBody
    public TaotaoResult deleteItemParam(@RequestParam("ids") Long ids){
        System.out.println(ids);
        TaotaoResult result=tbItemService.deleteItemParam(ids);
        return result;
    }
    //修改规格参数模板
    @RequestMapping("/updateItemParamByItemCatId/{cid}")
    @ResponseBody
    public TaotaoResult updateItemParamByItemCId(@PathVariable Long cid,String paramData) {
        TaotaoResult result = tbItemService.updateItemParamByItemCId(cid,paramData);
        return result;

    }
}
