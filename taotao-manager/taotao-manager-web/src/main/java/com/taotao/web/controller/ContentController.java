package com.taotao.web.controller;

import com.taotao.common.EasyUIResult;
import com.taotao.common.TaotaoResult;
import com.taotao.pojo.TbContent;
import com.taotao.service.ContentService;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yhk
 * @create 2019-11-15 19:02
 */
@Controller
@RequestMapping("/content")
public class ContentController {

    @Autowired
    private ContentService contentService;


    /**
     * 点击分类内容查询对应所有结果
     * @param categoryId
     * @param page
     * @param rows
     * @return
     */
    @RequestMapping("/query/list")
    @ResponseBody
    public EasyUIResult QueryContentList(Long categoryId,int page,int rows ) {
        //设置分页参数
        EasyUIResult result = contentService.QueryContentList(categoryId,page,rows);

        return result;
    }

    /**
     * 更新分类内容信息
     * @param contentCon
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public TaotaoResult updateContent(TbContent contentCon){
        TaotaoResult  result = contentService.updateContent(contentCon);
        return  result;
    }

    /**
     * 删除分类内容
     * @param ids
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public TaotaoResult deletConTent(String ids) {
        TaotaoResult result = null;
        List<String> delList = new ArrayList<String>();
        if (ids.contains(",")) {
            String[] strs = ids.split(",");
            for (String str : strs) {
                delList.add(str);
            }
            result = contentService.batchDeletsContent(delList);
        }else{
            result = contentService.deletConTent(ids);
        }
        return  result;
    }
    @RequestMapping("/save")
    @ResponseBody
    public TaotaoResult saveContent(TbContent content) {
        TaotaoResult result = contentService.saveContent(content);
        return result;
    }
}
