package com.taotao.web.controller;

import com.taotao.common.EasyUIResult;
import com.taotao.common.TaotaoResult;
import com.taotao.pojo.EasyUITreeNode;
import com.taotao.service.ContentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author yhk
 * @create 2019-11-15 11:44
 */

@Controller
@RequestMapping("/content/category")
public class ContentCategoryController {

    @Autowired
    private ContentCategoryService contentCategoryService;

    /**
     * 分类内容列表展示
     * @param parendid
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public List<EasyUITreeNode> getContentCategoryList(@RequestParam(value="id",defaultValue = "0")Long parendid) {
        List<EasyUITreeNode> list = contentCategoryService.contentCategoryService(parendid);
        return list;
    }

    /**
     * 新增子节点
     * @param parentId
     * @param name
     * @return
     */
    @RequestMapping("/create")
    @ResponseBody
    public TaotaoResult addNode(Long parentId,String name) {
        TaotaoResult result = contentCategoryService.addNode(parentId,name);
        return  result;
    }

    /**
     * 删除整个分类或者分类下的节点
     * @param id
     * @return
     */
    @RequestMapping("delete")
    @ResponseBody
    public TaotaoResult deleteNode(Long id) {
        TaotaoResult result = contentCategoryService.deleteNode(id);
        return result;
    }

    /**
     * 更新节点名称
     * @param id
     * @param name
     * @return
     */
    @RequestMapping("update")
    @ResponseBody
    public TaotaoResult updateNode(Long id,String name) {
        TaotaoResult result = contentCategoryService.updateNode(id,name);
        return result;
    }



}


