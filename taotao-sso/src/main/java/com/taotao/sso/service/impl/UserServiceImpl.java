package com.taotao.sso.service.impl;

import com.taotao.common.*;
import com.taotao.mapper.TbUserMapper;
import com.taotao.pojo.TbUser;
import com.taotao.pojo.TbUserExample;
import com.taotao.sso.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author yhk
 * @create 2019-11-21 10:17
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {
    /**
     * redis存储用户Token Key的统一前缀
     */
    @Value("${taotao.redis.login.token.prefix}")
    private String userTokenRedisKeyPrefix;

    @Autowired
    private TbUserMapper tbUserMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;
    /**
     * 注册资料校验
     * @param param  校验的数据格式
     * @param type  可选参数1,2,3代表 username,phone,email
     * @return
     */
    @Override
    public TaotaoResult checkData(String param, Integer type) {
        TaotaoResult taotaoResult = null;
        boolean result = false;
        switch (type) {
            case SystemConstants.TAOTAO_REGISTER_CHECK_TYPE_USERNAME:
                result = checkUserName(param);
                break;
            case SystemConstants.TAOTAO_REGISTER_CHECK_TYPE_PHONE:
                result = checkPhone(param);
                break;
            case SystemConstants.TAOTAO_REGISTER_CHECK_TYPE_EMAIL:
                result = checkEmail(param);
                break;
            default:
                 taotaoResult = TaotaoResult.build(500,"不支持检查的数据类型"+type+"");
        }
        if(taotaoResult == null) {
            if(result) {
                taotaoResult = TaotaoResult.ok(result);
            }else {
                taotaoResult = TaotaoResult.build(500,"数据已经存在");
                }
        }
        return taotaoResult;
    }

    /**
     * 用户注册
     * @param tbUser
     * @return
     */
    @Override
    public TaotaoResult register(TbUser tbUser) {
        if (tbUser == null) {
              return  TaotaoResult.build(500,"数据不能为空");
        }
        try {
            String md5Pwd = DigestUtils.md5Hex(tbUser.getPassword().getBytes());
            tbUser.setPassword(md5Pwd);

            //完善注册信息
            Date date = new Date();
            tbUser.setCreated(date);
            tbUser.setUpdated(date);

            //保存注册信息
            tbUserMapper.insert(tbUser);
            return TaotaoResult.ok();
        } catch (Exception e) {
            log.error("注册失败");
            return TaotaoResult.build(500,"保存用户失败");
        }
    }

    /**
     * 用户登录
     * @param username
     * @param password
     * @param request
     * @param response
     * @return
     */
    @Override
    public TaotaoResult login(String username, String password, HttpServletRequest request, HttpServletResponse response) {
       if(StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
           return TaotaoResult.build(500,"用户名或密码不能为空!!");
       }
        try {
            TbUserExample example = new TbUserExample();
            example.createCriteria().andUsernameEqualTo(username);
            List<TbUser> tbUsers = tbUserMapper.selectByExample(example);
            if(tbUsers == null || tbUsers.size() < 1) {
                return TaotaoResult.build(500,"用户名或者密码错误");
            }
            TbUser useInfo  = tbUsers.get(0);
            //生成用户的Token(也可以使用户JWT工具生成Token,安全性会更高)
            String token = IDUtils.genImageNameByUUID();
            //生成Redis的 Key
            String redisKey = getTokenRedisKey(token);
            //存储到Redis之前,将用户密码清除(可以降低安全)
            useInfo.setPassword(null);
            //将用户信息存到redis
            redisTemplate.opsForValue().set(redisKey, JsonUtils.objectToJson(useInfo));
            //设置Token的过期时间
            redisTemplate.expire(redisKey,30, TimeUnit.MINUTES);
            //将Token写入到Cookie
            CookieUtils.setCookie(request, response, "TT_TOKEN", token, (int)TimeUnit.MINUTES.toSeconds(30));
            return TaotaoResult.ok(token);
        } catch (Exception e) {
            log.error("登录失败", e);
            return TaotaoResult.build(500,"登录失败");
        }
    }

    /**
     * 根据Token查询用户信息
     * @param token
     * @return
     */
    @Override
    public TaotaoResult getUserByToken(String token) {
        if(StringUtils.isBlank(token)) {
            return TaotaoResult.build(500,"token不能为空！");
        }
        String redisKey = getTokenRedisKey(token);

        TbUser tbUser = null;
        try {
            String userJson = redisTemplate.opsForValue().get(redisKey);
            tbUser = JsonUtils.jsonToPojo(userJson, TbUser.class);
        } catch (Exception e) {
            log.error("获取用户信息失败, token: " + token, e);
        }
        if(tbUser == null) {
            return TaotaoResult.build(500,"获取用户信息失败");
        }
        return TaotaoResult.ok(tbUser);
    }

    @Override
    public TaotaoResult logout(String token, HttpServletRequest request, HttpServletResponse response) {
        //清除redis
        redisTemplate.expire(getTokenRedisKey(token),0,TimeUnit.SECONDS);
        //清除Cookie
        CookieUtils.deleteCookie(request,response,"TT_TOKEN");
        return TaotaoResult.ok();
    }


    /**
     * 检查邮箱
     * @param param
     * @return
     */
    private boolean checkEmail(String param) {
        TbUserExample example = new TbUserExample();
        example.createCriteria().andEmailEqualTo(param);
        List<TbUser> userList = tbUserMapper.selectByExample(example);

        //如果查出来数据,说明邮箱已经被其他用户绑定,需要返回false,告诉前台不能用此邮箱
        return !(userList != null && userList.size() > 0);
    }

    /**
     * 检查手机号
     * @param param
     * @return
     */
    private boolean checkPhone(String param) {
        TbUserExample example = new TbUserExample();
        example.createCriteria().andPhoneEqualTo(param);
        List<TbUser> userList = tbUserMapper.selectByExample(example);
        //如果查出来数据,说明手机已经被其他用户绑定了,需要返回false,告诉前台不能用此数据
        return !(userList != null && userList.size() > 0);
    }

    /**
     * 检查用户名
     * @param param
     * @return
     */
    private boolean checkUserName(String param) {
        TbUserExample example = new TbUserExample();
        example.createCriteria().andUsernameEqualTo(param);
        List<TbUser> userList = tbUserMapper.selectByExample(example);
        //如果查出来数据,说明手机已经被其他用户绑定了,需要返回false,告诉前台不能用此数据
        return !(userList != null && userList.size() > 0);
    }

    /**
     * 拼接唯一token
     */
    private String getTokenRedisKey(String token) {
        return userTokenRedisKeyPrefix + "-" + token;
    }
}
