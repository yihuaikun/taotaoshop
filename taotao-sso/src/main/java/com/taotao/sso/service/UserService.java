package com.taotao.sso.service;

import com.taotao.common.TaotaoResult;
import com.taotao.pojo.TbUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yhk
 * @create 2019-11-21 10:16
 */
public interface UserService{
    /**
     * 注册资料效验
     * @param param  校验的数据格式
     * @param type  可选1,2,3代表 username,phone,email
     * @return
     */
    public TaotaoResult checkData(String param,Integer type);

    /**
     * 用户注册
     * @param tbUser
     * @return
     */
    public TaotaoResult register(TbUser tbUser);

    /**
     * 用户登录
     * @param username
     * @param password
     * @param request
     * @param response
     * @return
     */
    public TaotaoResult login(String username, String password, HttpServletRequest request, HttpServletResponse response);

    /**
     * 根据Token查询用户信息
     * @param token
     * @return
     */
    public TaotaoResult getUserByToken(String token);

    /**
     * 用户退出
     * @param token
     * @return
     */
    TaotaoResult logout(String token,HttpServletRequest request,HttpServletResponse response);
}
