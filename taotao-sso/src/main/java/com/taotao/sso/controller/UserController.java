package com.taotao.sso.controller;

import com.taotao.common.JsonUtils;
import com.taotao.common.TaotaoResult;
import com.taotao.pojo.TbUser;
import com.taotao.sso.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yhk
 * @create 2019-11-21 16:33
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 检查注册信息
     * @param param
     * @param type
     * @return
     */
    @RequestMapping("/check/{param}/{type}")
    public TaotaoResult checkData(@PathVariable String param,@PathVariable Integer type) {
        TaotaoResult taotaoResult = userService.checkData(param,type);
        return taotaoResult;
    }

    /**
     * 用户注册
     * @param tbUser
     * @return
     */
    @RequestMapping(value = "/register",method= RequestMethod.POST)
    public  TaotaoResult registerUser(TbUser tbUser) {
        TaotaoResult taotaoResult = userService.register(tbUser);
        return  taotaoResult;
    }
    /**
     * 用户登录
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public TaotaoResult registerUser(String username, String password, HttpServletRequest req, HttpServletResponse resp) {
        TaotaoResult taotaoResult = userService.login(username, password, req, resp);
        return taotaoResult;
    }

    /**
     * 用户退出
     * @param token
     * @return
     */
    @RequestMapping(value = "/logout/{token}", method = RequestMethod.GET)
    public TaotaoResult logoutUser(@PathVariable String token,HttpServletRequest request,HttpServletResponse response) {
        TaotaoResult taotaoResult = userService.logout(token,request,response);
        return taotaoResult;
    }

    /**
     * 按照Token获取user登录信息
     * @param token
     * @param callback
     * @return
     */
    @CrossOrigin(origins = "http://localhost:8082")
    @RequestMapping(value = "/token/{token}", method = RequestMethod.GET)
    public Object getUserByToken(@PathVariable String token, @RequestParam(required = false) String callback) {
        TaotaoResult taotaoResult = userService.getUserByToken(token);
        if(!StringUtils.isBlank(callback)) {
            return callback+"(" + JsonUtils.objectToJson(taotaoResult) +");";
        }
        return taotaoResult;
    }

}
