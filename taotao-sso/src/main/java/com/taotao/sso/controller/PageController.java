package com.taotao.sso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author yhk
 * @create 2019-11-21 19:25
 */
@Controller
@RequestMapping("/user")
public class PageController {
    @RequestMapping("/showLogin")
    public  String showLogin(String redirect, Model model) {
        model.addAttribute("redirect",redirect);
        return "login";
    }
    @RequestMapping("/showRegister")
    public  String showRegister(String redirect, Model model) {
        model.addAttribute("redirect",redirect);
        return "register";
    }
}
