package com.taotao;

import com.taotao.portal.interceptor.HtmlInterceptor;
import com.taotao.portal.interceptor.OrderInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyMVCConfiguration implements WebMvcConfigurer {
    @Autowired
    private HtmlInterceptor htmlInterceptor;
    @Autowired
    private OrderInterceptor orderInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //registry.addInterceptor(htmlInterceptor).addPathPatterns("/**");
        registry.addInterceptor(orderInterceptor).addPathPatterns("/order/**");
    }
}