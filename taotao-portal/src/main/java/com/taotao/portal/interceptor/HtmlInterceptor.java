package com.taotao.portal.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class HtmlInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri = request.getRequestURI().toLowerCase();
        if(uri.endsWith(".html")) {
            uri = uri.substring(0, uri.lastIndexOf(".html"));
        } else {
            return true;
        }
        request.getRequestDispatcher(uri).forward(request, response);
        return false;
    }

}
