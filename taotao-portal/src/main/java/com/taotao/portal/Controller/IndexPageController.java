package com.taotao.portal.Controller;

import com.taotao.pojo.SearchResult;
import com.taotao.portal.Service.AdService;
import com.taotao.portal.Service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author yhk
 * @create 2019-11-16 10:53
 */
@Controller
public class IndexPageController {
    @Autowired
    private AdService adService;


    @Autowired
    private SearchService searchService;

    /**
     * 获取首页展示以及获取去广告列表
     * @param model
     * @return
     */
    @RequestMapping(path = {"/","/index"})
    public String showIndex(Model model) {
        String adResult = adService.getAdItemList();
        model.addAttribute("ad1",adResult);
        return  "index";
    }
    @RequestMapping("/search")
    public String search(@RequestParam("q") String keywords, @RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "30") Integer pageSize, Model model) {
        SearchResult searchResult = searchService.search(keywords, pageNum, pageSize);
        model.addAttribute("query", keywords);
        model.addAttribute("totalPages", searchResult.getTotalPages());
        model.addAttribute("page", searchResult.getRecordCount());
        model.addAttribute("itemList", searchResult.getItemList());
        return "search";
    }

}
