package com.taotao.portal.Controller;

import com.taotao.portal.Service.TbItemDescService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author yhk
 * @create 2019-11-20 19:14
 */
@Controller
public class TbItemDescController {
    @Autowired
    private TbItemDescService tbItemDescService;

    @RequestMapping("/item/desc/{id}")
    @ResponseBody
    public String getTbItemDesc(@PathVariable Long id) {
        String html = tbItemDescService.getTbItemDesc(id);
        return html;
    }

}
