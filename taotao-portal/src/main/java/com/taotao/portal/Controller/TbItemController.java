package com.taotao.portal.Controller;

import com.taotao.portal.Service.TbItemService;
import com.taotao.portal.pojo.TbItemExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author yhk
 * @create 2019-11-20 19:13
 */
@Controller
public class TbItemController {
    @Autowired
    private TbItemService tbItemService;

    @RequestMapping("/item/{id}")
    public String toItemDetailsPage(@PathVariable("id") Long id, Model model){
        TbItemExt tbItemExt = tbItemService.getTbItemInfo(id);
        model.addAttribute("item", tbItemExt);
        return "item";
    }
}
