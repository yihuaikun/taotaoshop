package com.taotao.portal.Service;

/**
 * @author yhk
 * @create 2019-11-20 19:15
 */
public interface TbItemDescService {
    /**
     * 获取服务层的商品描述信息
     * @param id
     * @return
     */
    String getTbItemDesc(Long id);
}
