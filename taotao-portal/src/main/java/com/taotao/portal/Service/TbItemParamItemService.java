package com.taotao.portal.Service;

/**
 * @author yhk
 * @create 2019-11-20 19:17
 */
public interface TbItemParamItemService {
    /**
     * 获取服务层的商品规格参数信息
     * @param id
     * @return
     */
    String getItemParam(Long id);
}
