package com.taotao.portal.Service;

import com.taotao.common.TaotaoResult;
import com.taotao.portal.pojo.Order;

/**
 * @author yhk
 * @create 2019-11-22 21:41
 */
public interface OrderService {
    public TaotaoResult createService(Order order);
}
