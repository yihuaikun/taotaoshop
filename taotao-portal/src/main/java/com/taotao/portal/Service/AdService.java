package com.taotao.portal.Service;

/**
 * @author yhk
 * @create 2019-11-16 10:55
 */
public interface AdService {
    /**
     * 首页大广告展示
     * @return
     */
    String getAdItemList();
}
