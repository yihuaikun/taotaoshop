package com.taotao.portal.Service;

import com.taotao.portal.pojo.TbItemExt;

/**
 * @author yhk
 * @create 2019-11-20 16:59
 */
public interface TbItemService {
    /**
     * 获取商品详细信息
     * @param id
     * @return
     */
    TbItemExt getTbItemInfo(Long id);
}
