package com.taotao.portal.Service;

import com.taotao.pojo.SearchResult;

/**
 * @author yhk
 * @create 2019-11-20 11:19
 */
public interface SearchService {
    /**
     * 获取搜索服务的搜索结果
     * @param keywords
     * @param pageNum
     * @param pageSize
     * @return
     */
    SearchResult search(String keywords, Integer pageNum, Integer pageSize);
}
