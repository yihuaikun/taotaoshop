package com.taotao.portal.Service;

import com.taotao.common.TaotaoResult;
import com.taotao.portal.pojo.TbItemExt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author yhk
 * @create 2019-11-22 14:39
 */
public interface CartService {
    /**
     * 添加商品到购物车
     * @param itemId
     * @param request
     * @param response
     * @return
     */
    public TaotaoResult addItem(Long itemId, HttpServletRequest request, HttpServletResponse response);

    /**
     * 取购物车列表
     * @param request
     * @return
     */
    public List<TbItemExt> getCartItemsList(HttpServletRequest request);

    /**
     * 修改指定商品数量
     * @param itemId
     * @param num
     * @param request
     * @param response
     * @return
     */
    public  TaotaoResult changeItemNum(long itemId,int num,HttpServletRequest request,HttpServletResponse response);

    /**
     * 删除购物车中的商品
     * @param itemId
     * @param request
     * @param response
     * @return
     */
    public List<TbItemExt> deleteItem(Long itemId, HttpServletRequest request, HttpServletResponse response);
}

