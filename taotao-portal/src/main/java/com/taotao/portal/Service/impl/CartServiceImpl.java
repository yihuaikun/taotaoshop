package com.taotao.portal.Service.impl;

import com.taotao.common.CookieUtils;
import com.taotao.common.HttpUtil;
import com.taotao.common.JsonUtils;
import com.taotao.common.TaotaoResult;
import com.taotao.portal.Service.CartService;
import com.taotao.portal.pojo.TbItemExt;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yhk
 * @create 2019-11-22 14:40
 */
@Service
public class CartServiceImpl implements CartService {

    //服务的url
//    @Value("${REST_BASE_URL}")
    @Value("${REST_BASE_URL}")
    private String REST_BASE_URL;
    //商品服务URL
    @Value("${ITEMS_ITEM_URL}")
    private String  ITEMS_ITEM_URL;
    //COOKIE中购物车商品对应的Key
//    @Value("${CART_ITEMS_LIST_key}")
    private String CART_ITEMS_LIST_key = "TT_CART";
    //购物车的cookie生存期
//    @Value("${CART_ITEMS_EXPIRE_TIME}")
    private Integer CART_ITEMS_EXPIRE_TIME = 9000000;



    /**
     * 添加商品到购物车
     * @param itemId
     * @param request
     * @param response
     * @return
     */
    @Override
    public TaotaoResult addItem(Long itemId, HttpServletRequest request, HttpServletResponse response) {
       //根据商品的id查询商品的信息
        TbItemExt item = getItemById(itemId);
        if (item == null){
            return TaotaoResult.build(400,"未查询到商品信息");
        }
        //取cookie中购物车的商品列表
        List<TbItemExt> cartItems = getItemListFromCookie(request);
        //判断该商品是否存在于购物车中
        boolean itemExists = false;
        for (TbItemExt cartItem : cartItems) {
            if(cartItem.getId().longValue() == itemId.longValue()) {
                //购物车中有次商品
                cartItem.setNum(cartItem.getNum() + 1);
                itemExists = true;
                break;
            }
        }
        if (!itemExists) {
            //设置数量为1
            item.setNum(1);
            //把商品添加到购物车
            cartItems.add(item);
        }

        //把购物车信息写入Cookie中
        CookieUtils.setCookie(request,response,CART_ITEMS_LIST_key,JsonUtils.objectToJson(cartItems),CART_ITEMS_EXPIRE_TIME,true);
        return TaotaoResult.ok(cartItems);
    }

    /**
     * 取购物车列表
     * @param request
     * @return
     */
    @Override
    public List<TbItemExt> getCartItemsList(HttpServletRequest request) {
       //取购物车信息
        List<TbItemExt> itemExtList = getItemListFromCookie(request);
        return itemExtList;
    }

    /**
     * 修改指定商品的数量
     * @param itemId
     * @param num
     * @param request
     * @param response
     * @return
     */
    @Override
    public TaotaoResult changeItemNum(long itemId, int num, HttpServletRequest request, HttpServletResponse response) {
       //从cookie中取商品列表
        List<TbItemExt> list = getItemListFromCookie(request);
        //从商品列表中找要是修改数量的商品
        for(TbItemExt item : list) {
            if(item.getId() == itemId) {
                //找到商品
                item.setNum(num);
                break;
            }
        }
        //把商品信息写入Cookie
        CookieUtils.setCookie(request,response,CART_ITEMS_LIST_key,JsonUtils.objectToJson(list),CART_ITEMS_EXPIRE_TIME,true);
        return TaotaoResult.ok();
    }

    /**
     * 删除购物车中的商品
     * @param itemId
     * @param request
     * @param response
     * @return
     */
    @Override
    public List<TbItemExt> deleteItem(Long itemId, HttpServletRequest request, HttpServletResponse response) {
        List<TbItemExt> itemList = getCartItemsList(request);
        //找到购物车中的商品并删除
        for(TbItemExt item : itemList) {
            if(item.getId().longValue() == itemId.longValue()) {
                itemList.remove(item);
                break;
            }
        }
        //更新Cookie中购物车的信息
        CookieUtils.setCookie(request, response, CART_ITEMS_LIST_key, JsonUtils.objectToJson(itemList), CART_ITEMS_EXPIRE_TIME, true);
        return itemList;
    }

    /**
     * 根据商品id获取关于商品的信息
     * @param itemId
     * @return
     */
    private TbItemExt getItemById(Long itemId) {
        //根据商品的id查询商品信息
        String resultStr = HttpUtil.doGet(REST_BASE_URL + ITEMS_ITEM_URL + itemId);
        //转换成taotaoResult
        TaotaoResult result = TaotaoResult.formatToPojo(resultStr, TbItemExt.class);
        //取商品信息
        TbItemExt item = null;
        if(result.getStatus() == 200) {
            item = (TbItemExt) result.getData();
        }
        return  item;
    }

    /**
     * 从cookie中获取原来已经存在的购物车商品信息
     * @param request
     * @return
     */
    private List<TbItemExt> getItemListFromCookie(HttpServletRequest request) {
        //取cookie中购物车商品列表
        String cartItemStr = CookieUtils.getCookieValue(request, CART_ITEMS_LIST_key, true);
        //如果不为空那么就转换为java对象
        List<TbItemExt> cartItems = null;
        if(!StringUtils.isBlank(cartItemStr)) {
            cartItems = JsonUtils.jsonToList(cartItemStr,TbItemExt.class);
        }else {
            cartItems = new ArrayList<>();
        }
        return cartItems;
    }
}
