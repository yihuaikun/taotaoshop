package com.taotao.portal.pojo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.taotao.pojo.TbItem;
public class TbItemExt extends TbItem {

    /**
     * 将数据库中image字段存储的,分隔格式的多张图片，转化成一个String数组
     *
     * @return
     */
    @JsonIgnore //序列化json时忽略次方法
    public String[] getImages() {
        if (this.getImage() != null && this.getImage() != "") {
            String[] strings = this.getImage().split(",");
            return strings;
        }
        return null;
    }
}
