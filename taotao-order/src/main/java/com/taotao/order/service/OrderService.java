package com.taotao.order.service;

import com.taotao.common.TaotaoResult;
import com.taotao.pojo.TbOrder;
import com.taotao.pojo.TbOrderItem;
import com.taotao.pojo.TbOrderShipping;

import java.util.List;

/**
 * @author yhk
 * @create 2019-11-22 19:35
 */
public interface OrderService {
    /**
     * 创建用户订单
     * @param order
     * @param itemList
     * @param orderShipping
     * @return
     */
    public TaotaoResult createOrder(TbOrder order, List<TbOrderItem> itemList, TbOrderShipping orderShipping);
}
