package com.taotao.order.service.impl;

import com.taotao.common.TaotaoResult;
import com.taotao.mapper.TbOrderItemMapper;
import com.taotao.mapper.TbOrderMapper;
import com.taotao.mapper.TbOrderShippingMapper;
import com.taotao.order.service.OrderService;
import com.taotao.pojo.TbOrder;
import com.taotao.pojo.TbOrderItem;
import com.taotao.pojo.TbOrderShipping;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author yhk
 * @create 2019-11-22 19:37
 */
@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class OrderServiceImpl implements OrderService {
    @Autowired
    private TbOrderMapper orderMapper;
    @Autowired
    private TbOrderItemMapper orderItemMapper;
    @Autowired
    private TbOrderShippingMapper orderShippingMapper;
    
    @Autowired
    private StringRedisTemplate redisTemplate;
    

    /**
     * redis中的Key
     */
    @Value("${ORDER_ID_KEY}")
    private String ORDER_ID_KEY;
    @Value("${ORDER_BEGIN_ID}")
    private Long ORDER_BEGIN_ID;

    @Override
    @Transactional(readOnly = false,propagation = Propagation.REQUIRED)
    public TaotaoResult createOrder(TbOrder order, List<TbOrderItem> itemList, TbOrderShipping orderShipping) {
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        //订单表
        //生成订单号
        String orderIdStr = ops.get(ORDER_ID_KEY);
        Long orderId = null;
        if(StringUtils.isBlank(orderIdStr)) {
            //如果redis中没有订单号使用订单号初始化
            ops.set(ORDER_ID_KEY,ORDER_BEGIN_ID.toString());
            orderId = ORDER_BEGIN_ID;
        }else {
            //生成订单号
            orderId = ops.increment(ORDER_ID_KEY);
        }
        //设置订单号
        order.setOrderId(orderId.toString());
        Date date = new Date();
        //订单创建时间
        order.setCreateTime(date);
        //订单更新时间
        order.setUpdateTime(date);
        //插入定单表
        orderMapper.insert(order);
        //插入订单商品表
        for (TbOrderItem tbOrderItem : itemList) {
            //取订单商品的Id
            Long orderItemId = ops.increment("ORDER_ITEM_ID");
            tbOrderItem.setId(orderItemId.toString());
            tbOrderItem.setOrderId(orderItemId.toString());
            //添加到订单商品表
            orderItemMapper.insert(tbOrderItem);
        }
        int i = 1 / 0;
        System.out.println(i);

        //插入物流表
        orderShipping.setOrderId(orderId.toString());
        orderShipping.setCreated(date);
        orderShipping.setUpdated(date);
        orderShippingMapper.insert(orderShipping);
        return TaotaoResult.ok(orderId.toString());
    }
}
