package com.taotao.service;

import com.taotao.common.TaotaoResult;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author yhk
 * @create 2019-11-14 19:51
 */

public interface ItemCatService {
    TaotaoResult getItemCat();
}
