package com.taotao.service;

import com.taotao.common.TaotaoResult;

/**
 * @author yhk
 * @create 2019-11-16 9:33
 */
public interface ContentService {
    /**
     *获取大广告
     * @param cid
     * @return
     */
    TaotaoResult getContentList(Long cid);
}
