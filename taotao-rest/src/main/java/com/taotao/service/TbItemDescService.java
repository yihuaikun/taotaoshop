package com.taotao.service;

import com.taotao.common.TaotaoResult;

/**
 * @author yhk
 * @create 2019-11-20 16:45
 */
public interface TbItemDescService {
    TaotaoResult getItemDesc(Long id);
}
