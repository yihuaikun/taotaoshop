package com.taotao.service;

import com.taotao.common.TaotaoResult;

/**
 * @author yhk
 * @create 2019-11-20 16:39
 */
public interface TbItemParamItemService {
    /**
     * 获取商品规格参数信息
     * @param id
     * @return
     */
    TaotaoResult getItemParam(Long id);
}
