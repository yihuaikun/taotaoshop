package com.taotao.service;

import com.taotao.common.TaotaoResult;

/**
 * @author yhk
 * @create 2019-11-18 19:22
 */
public interface CacheManagerService {
    /**
     * 清理緩存
     * @param key
     * @return
     */
    TaotaoResult clearCache(String key);

    TaotaoResult clearContentCache(Long contentCatId);
}
