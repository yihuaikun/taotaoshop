package com.taotao.service;

import com.taotao.common.TaotaoResult;

/**
 * @author yhk
 * @create 2019-11-20 15:52
 */
public interface TbItemService {
    /**
     * 获取商品的详细信息
     * @param id
     * @return
     */
    TaotaoResult getItemDetails(Long id);
}
