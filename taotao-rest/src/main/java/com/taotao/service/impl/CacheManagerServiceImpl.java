package com.taotao.service.impl;

import com.taotao.common.RedisKey;
import com.taotao.common.TaotaoResult;
import com.taotao.service.CacheManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author yhk
 * @create 2019-11-18 19:22
 */
@Service
public class CacheManagerServiceImpl implements CacheManagerService {
    @Autowired
    private StringRedisTemplate  redisTemplate;
    private String contentKeyPrefix = "taotao_content_cat_";
    @Override
    public TaotaoResult clearCache(String key) {
        try {
            redisTemplate.delete(key);
        } catch (Exception e) {
            e.printStackTrace();
//            return TaotaoResult.error("缓存清理失败: " + e.getMessage());
              return TaotaoResult.build(500,"缓存清理失败");
        }
        return TaotaoResult.ok();
    }

    @Override
    public TaotaoResult clearContentCache(Long contentCatId) {
        return clearCache(contentKeyPrefix + contentCatId);
    }
}
