package com.taotao.service.impl;

import com.taotao.common.ExceptionUtil;
import com.taotao.common.TaotaoResult;
import com.taotao.mapper.TbItemParamItemMapper;
import com.taotao.pojo.TbItemParamItem;
import com.taotao.pojo.TbItemParamItemExample;
import com.taotao.service.TbItemParamItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yhk
 * @create 2019-11-20 16:39
 */
@Service
public class TbItemParamItemServiceImpl implements TbItemParamItemService {
    @Autowired
    private TbItemParamItemMapper tbItemParamItemMapper;

    @Override
    public TaotaoResult getItemParam(Long id) {
        TbItemParamItem tbItemParamItem = null;
        try {
            TbItemParamItemExample condition = new TbItemParamItemExample();
            condition.createCriteria().andItemIdEqualTo(id);

            List<TbItemParamItem> tbItemParamItemList = tbItemParamItemMapper.selectByExampleWithBLOBs(condition);
            if (tbItemParamItemList == null || tbItemParamItemList.size() < 1) {
//                log.warn("没有查询到ID为: " + id+"的商品规格参数信息");
                return TaotaoResult.build(500, "没有查询到ID为" + id + "的商品规格参数信息");
            }else {
                tbItemParamItem = tbItemParamItemList.get(0);
                return TaotaoResult.ok(tbItemParamItem);
            }
        }catch (Exception e) {
            return TaotaoResult.build(500, "获取参数信息失败");
        }
    }
}

