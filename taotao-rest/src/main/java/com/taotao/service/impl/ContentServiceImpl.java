package com.taotao.service.impl;

import com.taotao.common.JsonUtils;
import com.taotao.common.RedisKey;
import com.taotao.common.TaotaoResult;
import com.taotao.mapper.TbContentMapper;
import com.taotao.pojo.TbContent;
import com.taotao.pojo.TbContentExample;
import com.taotao.service.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yhk
 * @create 2019-11-16 9:33
 */
@Service
public class ContentServiceImpl implements ContentService {
    @Autowired
    private TbContentMapper contentMapper;

    private String contentKeyPrefix = "taotao_content_cat_";

//    @Autowired
//    private JedisCluster jedisCluster;

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 从redis或者内存中查询大广告
     * @param cid
     * @return
     */
    @Override
    public TaotaoResult getContentList(Long cid) {
        //1.从redis中查询
        List<TbContent> tbContentList = getTbContentFromCache(cid);
        System.out.println("从redis中查询");
        //判断取出的数据是否为null
        if (tbContentList == null || tbContentList.size() == 0) {
            //从数据库中获取
            TbContentExample example = new TbContentExample();
            //添加条件
            TbContentExample.Criteria criteria = example.createCriteria();
            tbContentList = contentMapper.selectByExample(example);
            //放入redis缓存中,下次不再查询数据库
            pushTbContent2Cache(cid, tbContentList);
            System.out.println("从数据库查");
        }

        return TaotaoResult.ok(tbContentList);

    }

    private void pushTbContent2Cache(Long cid, List<TbContent> tbContentList) {
        //String s1 = JsonUtils.objectToJson(tbContentList);
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
            /*TbContent tbContent = tbContentList.get(0);
            ObjectMapper mapper = new ObjectMapper();
            String s = mapper.writeValueAsString(tbContent);
            String str = cid + "";*/
        ops.set(contentKeyPrefix+cid, JsonUtils.objectToJson(tbContentList));
        //jedisCluster.hset(RedisKey.taotao_content_cat_ID,cid+"",JsonUtils.objectToJson(tbContentList));
    }

    public List<TbContent> getTbContentFromCache(Long cid) {
     /*  List<TbContent> contentList = null;
        try {
            ValueOperations<String, String> ops = redisTemplate.opsForValue();
            String s = cid + "";
            String s1 = ops.get(s);
            ObjectMapper mapper = new ObjectMapper();
            TbContent tbContent = mapper.readValue(s1, TbContent.class);
            contentList = new ArrayList<>();
            contentList.add(tbContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;*/
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        String s = ops.get(contentKeyPrefix+cid);
        List<TbContent> contentList = JsonUtils.jsonToList(s, TbContent.class);
        return  contentList;
    }
        /*List<TbContent> list = null;
        //查询缓存
        String json = jedisCluster.hget(RedisKey.taotao_content_cat_ID, cid + "");
        //得到查询结果,把json转换为List返回
        if (StringUtils.isNotBlank(json)) {
            list = JsonUtils.jsonToList(json, TbContent.class);
        }
        return list;*/
}
