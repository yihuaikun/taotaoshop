package com.taotao.service.impl;

import com.taotao.common.ExceptionUtil;
import com.taotao.common.TaotaoResult;
import com.taotao.mapper.TbItemDescMapper;
import com.taotao.pojo.TbItemDesc;
import com.taotao.service.TbItemDescService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author yhk
 * @create 2019-11-20 16:45
 */
@Service
public class TbItemDescServiceImpl implements TbItemDescService {
    @Autowired
    private TbItemDescMapper tbItemDescMapper;
    @Override
    public TaotaoResult getItemDesc(Long id) {
        try {
            TbItemDesc tbItemDesc = tbItemDescMapper.selectByPrimaryKey(id);
            return TaotaoResult.ok(tbItemDesc);
        } catch (Exception e) {
//            return TaotaoResult.error("查询商品描述信息失败");
            return  TaotaoResult.build(500,"查询商品描述信息失败");
        }

    }

}
