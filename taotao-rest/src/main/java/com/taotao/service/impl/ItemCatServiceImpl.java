package com.taotao.service.impl;

import com.taotao.common.TaotaoResult;
import com.taotao.domian.ItemCat;
import com.taotao.mapper.TbItemCatMapper;
import com.taotao.pojo.TbItemCat;
import com.taotao.pojo.TbItemCatExample;
import com.taotao.service.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yhk
 * @create 2019-11-14 20:01
 */
@Service
public class ItemCatServiceImpl  implements ItemCatService {
    @Autowired
    private TbItemCatMapper tbItemCatMapper;


    @Override
    public TaotaoResult getItemCat() {
        List<Object> itemCatList = loadItemCat(0L);
        return TaotaoResult.ok(itemCatList);
    }

    private List<Object> loadItemCat(Long parentId) {
        //创建一个list集合用来存放查询出来的分类数据
        List<Object> result = new ArrayList<>();
        TbItemCatExample condition = new TbItemCatExample();
        //添加查询添加,查询状态不等于2,和父id等于0的
        condition.createCriteria().andStatusNotEqualTo(2).andParentIdEqualTo(parentId);


        List<TbItemCat> tbItemCatList = tbItemCatMapper.selectByExample(condition);
        int count = 0;
        //遍历查询结果
        for (TbItemCat tbItemCat : tbItemCatList) {
            //获取父id
            if (tbItemCat.getIsParent()) {
                //创建分类对象
                ItemCat itemCat = new ItemCat();

                itemCat.setUrl("/products/" + tbItemCat.getId() + ".html");
                if (parentId.equals(0)) {
                    itemCat.setName("<a href='/products/" + tbItemCat.getId() + ".html'>" + tbItemCat.getName() + "</a>");
                } else {
                    itemCat.setName(tbItemCat.getName());
                }
                itemCat.setItem(loadItemCat(tbItemCat.getId()));
                result.add(itemCat);
                count++;
                if (count >= 14) {
                    break;
                }
            } else {
                String itemCat = "/products/" + tbItemCat.getId() + ".html|" + tbItemCat.getName();
                result.add(itemCat);
            }

        }
        return result;
    }
}
