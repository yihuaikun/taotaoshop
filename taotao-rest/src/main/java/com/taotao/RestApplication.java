package com.taotao;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author yhk
 * @create 2019-11-14 19:47
 */
@SpringBootApplication
@MapperScan("com.taotao.mapper")
public class RestApplication {
    public static void main(String[] args) {

        SpringApplication.run(RestApplication.class,args);
    }
}
