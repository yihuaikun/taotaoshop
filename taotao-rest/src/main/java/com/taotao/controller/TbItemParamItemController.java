package com.taotao.controller;

import com.taotao.common.TaotaoResult;
import com.taotao.service.TbItemParamItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author yhk
 * @create 2019-11-20 16:36
 */
@Controller
public class TbItemParamItemController {
    @Autowired
    private TbItemParamItemService tbItemParamItemService;

    /**
     * 获取商品规格参数信息
     * @param id
     * @return
     */
    @RequestMapping("/item/param/{id}")
    @ResponseBody
    public TaotaoResult getItemParam(@PathVariable Long id) {
        TaotaoResult result = tbItemParamItemService.getItemParam(id);
        return result;
    }


}
