package com.taotao.controller;

import com.taotao.common.TaotaoResult;
import com.taotao.service.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author yhk
 * @create 2019-11-14 19:49
 */
@Controller
@RequestMapping("/rest/itemcat")
public class ItemCatController {
    @Autowired
    private ItemCatService itemCatService;

    /**
     * 分类展示
     * @return
     */
    @CrossOrigin
    @RequestMapping("/all")
    @ResponseBody
    public Object getItemCat() {
        TaotaoResult result = itemCatService.getItemCat();
        return  result;
    }
}
