package com.taotao.controller;

import com.taotao.common.TaotaoResult;
import com.taotao.service.CacheManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yhk
 * @create 2019-11-18 19:11
 */
@RestController
@RequestMapping("/cache")
public class CacheController {

    @Autowired
    private CacheManagerService cacheManagerService;

    @GetMapping("/clear/{key}")
    public TaotaoResult clearCache(@PathVariable String key) {
        return cacheManagerService.clearCache(key);
    }
    @GetMapping("/clear/content/cat/{contentCatId}")
    public TaotaoResult clearContetCache(@PathVariable  Long contentCatId)  {

        return cacheManagerService.clearContentCache(contentCatId);
    }
}
