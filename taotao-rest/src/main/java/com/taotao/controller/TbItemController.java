package com.taotao.controller;

import com.taotao.common.TaotaoResult;
import com.taotao.service.TbItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yhk
 * @create 2019-11-20 15:48
 */
@RestController
@RequestMapping("/item")
public class TbItemController {
    @Autowired
    private TbItemService tbItemService;

    /**
     * 获取商品详细信息
     * @param id
     * @return
     */
    @RequestMapping("/get_item_detail/{id}")
    public TaotaoResult getItemDetails(@PathVariable Long id) {
        TaotaoResult result = tbItemService.getItemDetails(id);
        return  result;
    }

}
